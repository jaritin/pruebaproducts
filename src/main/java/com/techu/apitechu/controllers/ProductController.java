package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    //CRUD Create Read Update Delete
    //HEAD OPTIONS

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;

    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }

        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("La descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    //Actualización total
    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro URL es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    //Actualización parcial
    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updatePatchProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updatePatchProduct");
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        ProductModel patchProduct = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");

                patchProduct = productInList;

                if (product.getPrice() != 0) {
                    productInList.setPrice(product.getPrice());
                    System.out.println("El precio del actualizada es " + product.getPrice());
                }
                if (product.getDesc() != null) {
                    productInList.setDesc(product.getDesc());
                    System.out.println("La descripción del actualizada es " + product.getDesc());
                }
            }
        }

        return patchProduct;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel deleteProduct = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                foundCompany = true;
                deleteProduct = productInList;
            }
        }

        if (foundCompany == true){
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(deleteProduct);
        }

        return deleteProduct;
    }

}